
package com.java.cukes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.java.cukes.Hooks;
import com.java.cukes.Log;
import com.java.cukes.Utilities;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;


public class targetsSteps {	
	public static WebDriver driver;
	private String testdriveURL;
	public static String currentDate= Utilities.dateForTodayTomorrowAndYestderday("Today");
	public static String firstname = "Automation"+Utilities.rndmNumGen(1, 100000);
	//public static String lastName = "Automation"+Utilities.rndmNumGen(1, 100000);
	//	public static String firstname = "Automation12366";
	public static String commEmailID = "cucumber.automation+"+Utilities.rndmNumGen(1, 100000)+"@gmail.com";
	public static String scheduleName = "ScheduleAutomation"+Utilities.rndmNumGen(1, 100000);
	


	public targetsSteps()
	{
		testdriveURL = System.getenv("TESTDRIVE_URL");
		if(testdriveURL == null){
			testdriveURL = System.getenv("TEST_URL");
		}
		driver = Hooks.driver;
	}

	@Given("^I am on login page$")
	public void I_am_on_testdrive_login_page() throws Throwable {		
		driver.get("https://solution-qa.7targets.com/");
	}

	@Given("^I am on gmail login page$")
	public void gmail_login_page() throws Throwable {		
		driver.get("https://www.gmail.com");
	}

	@Given("^I enter username as \"([^\"]*)\" and password as \"([^\"]*)\"$") 
	public void I_enter_username_as_and_password_as(String username, String password) throws Throwable {
		driver.findElement(By.id("loginEmail")).sendKeys(username);
		driver.findElement(By.id("loginPassword")).sendKeys(password);
	}

	@When("^I click on login button$")
	public void I_click_on_login_button() throws Throwable{
		driver.findElement(By.xpath("//*[@id=\"react-tabs-1\"]/div/form/div[4]/input")).click();
		Thread.sleep(3000);
	}

	@When("^I logout from testdrive$")
	public void I_logout_from_testdrive() throws Throwable{
		driver.findElement(By.xpath("(//img[@title='Logoff'])[1]")).click();
	}

	@Then("^I should be on \"([^\"]*)\" page$")
	public void I_should_be_on_page(String page) throws Throwable {
		if (page.equals("Dashboard")) {
			assertEquals(driver.findElement(By.xpath("(//img[@alt='dashboard_icon']/following-sibling::p)[2]")).getText(),page);
		}
		else if (page.equals("Leads")) {
			assertEquals(driver.findElement(By.xpath("//img[@alt='leads_icon']/following-sibling::p")).getText(),page);	    	
		}	
	}

	@Then("^I verify \"([^\"]*)\" message$") 
	public void I_verify_on_page(String error) throws Throwable {
		assertEquals(driver.findElement(By.xpath("//font[contains(text(),'Authentication failed!')]")).getText(),error);
	}

	@When ("^I navigate to \"([^\\\"]*)\" page$")
	public void I_navigate_to_leades_page(String page) {
		if (page.equals("Leads")) {
			driver.findElement(By.xpath("//p[text()='Leads']")).click();
		}
		else if (page.equals("Assistant")) {
			driver.findElement(By.xpath("//img[@alt='assistant_icon']/following-sibling::p")).click();
		}
		//Utilities.sleepTime();

	}
	@When("^I navigate to Leades page$")
	public void i_navigate_to_Leades_page() throws Throwable {

		driver.findElement(By.xpath("//p[text()='Leads']")).click();

	}

	@And("^I add \"([^\"]*)\" Leads to Leads page$")
	public static void add_leads(String leadStatus) throws InterruptedException {
	//String firstname = "Automation"+Utilities.rndmNumGen(1, 100000)+"@gmail.com";
	int num = Utilities.rndmNumGen(1, 100000);
		Utilities.clickOnElement("(//div[text()='Add Lead'])[1]");
		System.out.println("*******"+currentDate);
		Utilities.enterText("fname", firstname);
		Utilities.enterText("lname", "User");
		
		if(leadStatus.contains("deactivated")) {
			//String emailID = "cucumber.automation+"+Utilities.rndmNumGen(1, 100000)+"@gmail.com";
		//	System.out.println("Email Id is ***************"+emailID);
			Utilities.enterText("email", Utilities.generateRandomEmail());
		}
		else if(leadStatus.contains("Unsubscribed")){
			String emailID = "cucumber.automation+"+Utilities.rndmNumGen(1, 100000)+"@gmail.com"; //updated
			Utilities.enterText("email", "commEmailID");
		}
		else {
			String emailID = "cucumber.automation+"+Utilities.rndmNumGen(1, 100000)+"@gmail.com";
			driver.findElement(By.xpath("//input[@name='email']")).sendKeys(emailID);
			//Utilities.enterText("email", emailID);
		}
	//	Utilities.enterText("numberCountryCode", "91");
		//Utilities.enterText("react-select-3-input", "AWS Event");
		//Utilities.enterTextUsingXpath("(//input[@name='context'])[1]","AWS Event");
		
		driver.findElement(By.xpath("//input[@name='phoneno']")).sendKeys("numberCountryCode", "91","number","1234567890");
		
		//			 Utilities.clickOnElement("//input[contains(@id,'react-select') and @aria-autocomplete='list']");//Changed
		//			 Utilities.sleepTime();
		//			 Utilities.enterTextUsingXpath("//input[contains(@id,'react-select') and @aria-autocomplete='list']", "Cloud Migration");//
		//		 	 Utilities.selectDropDownValue("//input[contains(@id,'react-select') and @aria-autocomplete='list']", "Cloud Migration");//
				Utilities.clickOnElement("//*[@id=\"left-details\"]/div[5]/div/button[1]/div");
		sleepTime();

	}

	@And("^I search for created lead$")
	public static void serachForLead() throws InterruptedException {
		Utilities.enterTextUsingXpath("//input[@class='add-leads-search-input']","TestUser13June");
	}

	@Then("^I validate lead created$")
	public static void validate_Leads() throws AWTException {
		Utilities.clickOnElement("//input[contains(@placeholder,'Search')]");
		System.out.println(firstname);
		Utilities.enterTextUsingXpath("//input[contains(@placeholder,'Search')]",firstname);
		Robot robo=new Robot();
		robo.keyPress(KeyEvent.VK_ENTER);
		robo.keyRelease(KeyEvent.VK_ENTER);
		//String username []= driver.findElement(By.xpath("(//div[@class='all-leads-block_1'])[2]/span")).getText().split(" ");
		//assertEquals(username[0],firstname);
		
	driver.findElement(By.xpath("//i[@class='fas fa-search']")).click();
		sleepTime();

	}

	@And("^I click on created lead$")
	public static void click_on_lead() {
		Utilities.clickOnElement("(//div[@class='all-leads-block_1'])[2]/span");
		sleepTime();

	}

	@And("^I click on \"([^\"]*)\" button on lead page$")
	public static void clickOnUnsubscribeButton(String buttonName) {
		if(buttonName.contains("Unsubscribe")) {
			Utilities.clickOnElement("//button[text()='"+buttonName+"']");
			Utilities.clickOnElement("//button[text()='Yes']");
			Hooks.driver.navigate().refresh();
		//	validate_Leads();
		}
		else if(buttonName.contains("Message")) {
			Utilities.clickOnElement("//button[text()='"+buttonName+"']");

		}
	}


	@Then("^I validate \"([^\"]*)\" status on created lead$")
	public static void validate_status(String status) {
		//Hooks.driver.navigate().refresh();
		sleepTime();
		 status=Hooks.driver.findElement(By.xpath("(//h5[@class='font-24-letterSpacing-40']/ancestor::td)[1]")).getText();
		 System.out.println("actual status is="+status);
		if (status.equalsIgnoreCase("cold"))
		{
		
		 System.out.println("actual status is="+status);
		Assert.assertEquals(status, "Cold");
		}
		else if (status.equalsIgnoreCase("Deactivate"))
		{
			 System.out.println("actual status is="+status);
				Assert.assertEquals(status, "Deactivate");
		}
		else if (status.equalsIgnoreCase(" unsubscribe"))
		{
			 System.out.println("actual status is="+status);
				Assert.assertEquals(status, " unsubscribe");
		}
		
	}

	@And("^I wait for \"([^\"]*)\" min to change status$")
	public static void wait_for_status_change(int index) throws InterruptedException {
		//	 Hooks.driver.manage().timeouts().implicitlyWait(35,TimeUnit.MINUTES);
		//	 Utilities.waitForElementPresence(By.xpath("(//span[text()='7'])[1]"), 600);
		while(Hooks.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS) != null) {
			Hooks.driver.navigate().refresh();
			//validate_Leads();
			String followUpText = Utilities.getElementText(By.xpath("//div[contains(@class,'signup-password-progressbar')]/div/span"));
			if(followUpText.contains("7 of 7 Follow-ups Completed")) {
				break;
			}
		}
		//	 Thread.sleep(25000);
		//	 Hooks.driver.navigate().refresh();
		//	 validate_Leads();
		//// Step %{"I validate lead created and its status on leads page"}
		//	 String followUpText = Utilities.getElementText(By.xpath("//div[contains(@class,'signup-password-progressbar')]/div/span"));
		//	 assertEquals(followUpText, "7 of 7 Follow-ups Completed");
		//// Utilities.waitForElementPresence("");
	}



	@And("^I enter details to gmail page$")
	public static void enter_gmail_details() throws InterruptedException {
		Utilities.enterText("identifierId", "cucumber.automation@gmail.com");
		Utilities.clickOnElement("//span[contains(text(),'Next')]");
		Utilities.enterTextUsingXpath("//input[@name='password']", "automation");
		Utilities.clickOnElement("//span[contains(text(),'Next')]");
	}
	

	@When("^I navigate to Sign Up link$")
	public static void click_on_sign_up_link() {
		Utilities.clickOnElement("//li[@class='react-tabs__tab react-tabs__tab--selected']");
		//sleepTime();//"//li[@class='react-tabs__tab react-tabs__tab--selected']");
	}

	@And("^I entered sign up details$")
	public static void sign_up_details() {
		Utilities.clickOnElement("//li[@id='react-tabs-2']");
		Utilities.enterText("signupFname", firstname);
		Utilities.enterText("signupLname", "User");
		String emailID = "cucumber.automation+"+Utilities.rndmNumGen(1, 100000)+"@gmail.com";
		Utilities.enterText("signupEmail", emailID);
		Utilities.enterText("signupPassword", "Opex@123");
		Utilities.enterText("signupConfirmPassword", "Opex@123");
		Hooks.driver.switchTo().frame(0);
		//	 Utilities.clickOnElement("//label[contains(text(),'not a robot')]");
		Utilities.clickOnElement("//div[@class='recaptcha-checkbox-border']");

		Utilities.clickOnElement("//input[@value='Sign Up']");

	}

	@Then("^I validate add lead page display error message$")
	public static void validate_error_msg() {
		String errorMsg = Utilities.getElementText(By.xpath("//div[@class='is-invalid']"));
		System.out.println(errorMsg);
		assertEquals(errorMsg, "Lead having this email id is unsubscribed for your organisation.");
	}

	@Then("^I validate pagination of all leads page$")
	public static void validate_pagination() {
		int pageCount = Utilities.countRow(By.xpath("//li[contains(@class,'paginaion')]"));
		for(int i=1;i<=pageCount;i++) {
			Utilities.clickOnElement("(//li[contains(@class,'paginaion')])["+i+"]");
			sleepTime();
			Boolean rowPresence = Utilities.isElementPresent(By.xpath("//div[@class='all-leads-block']"));
			assertTrue(rowPresence);
		}
	}

	@Then("^I validate message lead page$")
	public static void validate_message() {
		//	 String monthName= Utilities.currentMonth();
		Utilities.clickOnElement("(//div[contains(@class,'all-leads-expanded-block')]//h5[contains(text(),'"+firstname+"')]/parent::div/following-sibling::div[2]//div/h6)[1]");
		sleepTime();
		String msgText = Utilities.getElementText(By.xpath("//div[@class='timelineMessageBlockInner']//div/p"));
		assertNotNull(msgText);
		System.out.println(msgText);
		Utilities.hitEscapeKey();
//		Utilities.clickOnElement("//h1[text()='All Leads']");
		sleepTime();
	}

	@Then("^I validate all message lead page$")
	public static void validateAllMessages() {
		String allMsgText = Utilities.getElementText(By.xpath("(//div[@class='individual-chat-img-name-d-flex-content-right']/p)[1]"));
		assertNotNull(allMsgText);
	}

	@And("^I click on first lead$")
	public static void clickOnFirstLead() {
		Utilities.clickOnElement("(//div[@class='all-leads-block'])[1]");
		sleepTime();
	}

	@Then("^Validate details on lead details page$")
	public static void validateDetailsOnLeadDetailsPage() {
		String statusTextPresence = Utilities.getElementText(By.xpath("(//table[@class='all-leads-text-content-table'])[1]//tr[2]/td[2]"));
		assertNotNull(statusTextPresence);
		Boolean timelinePresence =Utilities.isElementPresent(By.xpath("(//div[contains(@class,'all-leads-gray-bg-graph')])[1]"));
		if(timelinePresence.equals(true))
			assertTrue(timelinePresence);
		else
			Log.info("Timeline is not present on the page");
	}

	@And("^I filter with hot leads and validate status$")
	public static void filterWithHotLeads() {
		Utilities.clickOnElement("//button[@class='btn add_lead' and text()='All']");
		Utilities.clickOnElement("//div[@class='dropdown-menu show']/button[text()='Hot']");
		sleepTime();
		int leadsRowCount = Utilities.countRow(By.xpath("//div[@class='all-leads-block']"));
		for(int i=1;i<=3;i++) {
			String status = Utilities.getElementText(By.xpath("(//div[@class='all-leads-block'])["+i+"]/div[4]/span"));
			Assert.assertEquals(status,"Hot");
		}
	}

	@And("^I click on Restart Followup button$")
	public static void clickOnRestartFollowup() {
		Utilities.clickOnElement("(//button[text()='Restart Followup'])[1]");
		sleepTime();
	}

	@Then("^I validate prepopulated fields$")
	public static void validatePrepopulatedFields() {
		String firstname = Utilities.getElementText(By.name("fname"));
		assertNotNull(firstname);
		String email = Utilities.getElementText(By.name("email"));
		assertNotNull(email);

	}

	@And("^I updated values to edit lead page$")
	public static void updateValuesToEditLead() {
		Utilities.clearText(By.xpath("//input[@name='fname']"));
		Utilities.enterTextUsingXpath("//input[@name='fname']",firstname);
		System.out.println("Updated firstname is:"+firstname);
		//Utilities.clickOnElement("(//div[contains(@class,'add-assistant-name-input-dropdown')])[1]");
		Utilities.clearText(By.xpath("(//input[@name='context'])[1]"));
		Utilities.enterTextUsingXpath("(//input[@name='context'])[1]", "AWS Event");
	}

	@Then("^I validate Re-followup for same lead$")
	public static void validateReFollowup() {
		Boolean nextFollowupText = Utilities.isElementPresent(By.xpath("//h6[text()='Next Follow-up']"));
		assertTrue(nextFollowupText);

		String statusText = Utilities.getElementText(By.xpath("//td/h6[text()='Status']/parent::td/parent::tr/td[2]/h5"));
		assertEquals(statusText, "Cold");

		//		JavascriptExecutor jse = (JavascriptExecutor) Hooks.driver;     
		//		jse.executeScript("document.querySelector('table th:last-child').scrollIntoView();");

		Boolean reFollowupText = Utilities.isElementPresent(By.xpath("//span[contains(text(),'Re-Follow up')]"));
		assertTrue(reFollowupText);

	}

	@And("^I create new schedule$")
	public static void createSchedule() {
		Utilities.clickOnElement("//button[text()='Schedule']");
		sleepTime();
		Utilities.enterTextUsingXpath("//div[contains(@class,'add-assistant-name-input-dropdown')]/div/div/div",scheduleName);
		Utilities.enterText("scheduleDescription", "Schedule For Automation");
		Utilities.clickOnElement("//button[text()='Save']");
		sleepTime();
	}

	@And("^I add lead with created schedule$")
	public static void createLeadWithSchedule() {
		Utilities.clickOnElement("//button[contains(text(),'Add Lead')]");
		System.out.println("*******"+currentDate);
		Utilities.enterText("fname", firstname);
		Utilities.enterText("lname", "User");
		Utilities.enterText("numberCountryCode", "91");
		Utilities.enterText("number", "1234567890");
		Utilities.enterText("react-select-3-input", "AWS Event");
		driver.findElement(By.xpath("//input[@name='phoneno']")).sendKeys("numberCountryCode", "91","number", "1234567890");
		Utilities.enterTextUsingXpath("(//input[@name='context'])[1]", "AWS Event");
		Utilities.clickOnElement("(//button[text()='Schedule'])[1]");
		sleepTime();
	}
	@Given("^I am on testdrive login page$")
	public void i_am_on_testdrive_login_page() throws Throwable {
		driver.get("https://solution-qa.7targets.com/");

	}

	@When("^I navigate to Leads page$")
	public void i_navigate_to_Leads_page() throws Throwable {
	}
	
	
	@When("^I \"([^\"]*)\" New lead$")
	public void i_New_lead(String arg1) throws Throwable {
	    Utilities.clickOnElement("(//div[@class='all-leads-block'])[1]");
	    Utilities.clickOnElement("//button[text()='More']");
	    Utilities.clickOnElement("//button[text()='Deactivate']");//button[text()='Yes']//button[text()='Yes']
	    driver.findElement(By.xpath("//textarea[@placeholder='Write Notes here...']")).sendKeys("Deactivate");
	    Utilities.clickOnElement("//button[text()='Yes']");
	    Utilities.clickOnElement("(//div[@class='all-leads-block'])[1]");
	}
	@When("^I add Leads to Leads page$")
	public void i_add_Leads_to_Leads_page() throws Throwable {
		Utilities.clickOnElement(" ");
		
	    
}

	static void sleepTime(){
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
